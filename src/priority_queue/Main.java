package priority_queue;

public class Main {
    public static void main(String[] args) {
        MaxPriorityQueue queue = new MaxPriorityQueue(new int[]{4, 16, 14, 7, 1, 2, 8, 3, 10, 9});
        queue.displayMaxQueue();

        System.out.println(queue.queueMaximum());

        queue.queueIncreaseKey(3, 11);
        queue.displayMaxQueue();

        queue.queueInsertKey(19);
        queue.displayMaxQueue();

        System.out.println(queue.queueExtractMaximum());
        queue.displayMaxQueue();
    }
}
