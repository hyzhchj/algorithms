package dp.practice6;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String word1 = "abcfh";
        String word2 = "bajhf";
        System.out.println(minimumDistance(word1, word2));
    }

    private static int minimumDistance(String word1, String word2) {
        if (word1 == null || word2 == null) {
            return 0;
        }
        int len1 = word1.length();
        int len2 = word2.length();
        if (len1 == 0 || len2 == 0) {
            return Math.max(len1, len2);
        }
        int[][] count = new int[len1 + 1][len2 + 1];
        for (int i = 0; i <= len1; i++) {
            count[i][0] = i;
        }
        for (int i = 0; i <= len2; i++) {
            count[0][i] = i;
        }
        for (int i = 1; i <= len1; i++) {
            for (int j = 1; j <= len2; j++) {
                if (word1.charAt(i - 1) == word2.charAt(j - 1)) {
                    //不需要操作
                    count[i][j] = count[i - 1][j - 1];
                } else {
                    //改、删、增
                    count[i][j] = min(count[i - 1][j - 1], count[i - 1][j], count[i][j - 1]) + 1;
                }
            }
        }
        List<int[]> list = Arrays.asList(count);
        list.forEach(e -> {
            System.out.println(Arrays.toString(e));
        });
        return count[len1][len2];
    }

    private static int min(int... nums) {
        int min = nums[0];
        for (int num : nums) {
            if (num < min) {
                min = num;
            }
        }
        return min;
    }
}
