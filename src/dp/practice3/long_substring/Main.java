package dp.practice3.long_substring;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String A = "aabdc";
        String B = "efaaj";
        System.out.println(longSubstring(A, B));
    }

    private static int longSubstring(String A, String B) {
        if (A == null || B == null || A.length() == 0 || B.length() == 0) {
            return 0;
        }
        int lenOfA = A.length();
        int lenOfB = B.length();
        //状态记录结构
        int[][] count = new int[lenOfB][lenOfA];
        //初始状态
        int max = 0;
        for (int i = 0; i < lenOfA; i++) {
            if (A.charAt(i) == B.charAt(0)) {
                count[0][i] = 1;
                max = 1;
            }
        }
        for (int i = 1; i < lenOfB; i++) {
            for (int j = 0; j < lenOfA; j++) {
                //状态转移
                if (B.charAt(i) == A.charAt(j)) {
                    if (j - 1 >= 0) {
                        count[i][j] = count[i - 1][j - 1] + 1;
                    } else {
                        count[i][j] = 1;
                    }
                    max = Math.max(max, count[i][j]);
                }
            }
        }
        List<int[]> list = Arrays.asList(count);
        list.forEach(e -> {
            System.out.println(Arrays.toString(e));
        });
        return max;
    }
}
