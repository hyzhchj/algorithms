/**
 * 公共子串
 *
 * 给出两个字符串，找到最长公共子串，返回其长度
 */
package dp.practice3.long_substring;