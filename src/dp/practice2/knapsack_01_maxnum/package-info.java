/**
 * 背包问题1：最大数量
 *
 * 在n个物品中挑选若干物品装入背包，最多能装多少？假设背包的大小为m，每个物品的大小为A[i]
 */
package dp.practice2.knapsack_01_maxnum;