/**
 * 背包问题2：最大价值
 *
 * 给出n个物品的体积A[i]和其价值V[i]，将他们装入一个大小为m的背包，最多能装入的总价值有多大？
 */
package dp.practice2.knapsack_01_maxvalue;