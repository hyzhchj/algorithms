package dp.practice2.knapsack_01_maxsize;

import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        int m = 10;
        int[] goods = new int[]{1, 4, 2, 3, 6};
        System.out.println(knapsack(m, goods));
    }

    public static int knapsack(int m, int[] goods) {
        if (goods == null || goods.length == 0 || m == 0) {
            return 0;
        }
        int len = goods.length;
        int[][] count = new int[len][m + 1];
        //初始状态
        for (int i = 0; i < m + 1; i++) {
            if (i >= goods[0]) {
                count[0][i] = goods[0];
            }
        }
        for (int i = 1; i < len; i++) {
            for (int j = 1; j < m + 1; j++) {
                if (j >= goods[i]) {
                    count[i][j] = Math.max(count[i - 1][j], count[i - 1][j - goods[i]] + goods[i]);
                } else {
                    count[i][j] = count[i - 1][j];
                }
            }
        }
        List<int[]> list = Arrays.asList(count);
        list.forEach(e -> {
            System.out.println(Arrays.toString(e));
        });
        return count[len - 1][m];
    }
}
