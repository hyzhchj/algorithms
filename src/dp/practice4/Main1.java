package dp.practice4;

import java.util.Arrays;

public class Main1 {
    public static void main(String[] args) {
        int[] moneys = new int[]{9, 7, 3, 10, 1, 5, 2};
        System.out.println(Arrays.toString(moneys));
        System.out.println(rob(moneys));
    }

    public static int rob(int[] moneys) {
        if (moneys == null || moneys.length == 0) {
            return 0;
        }
        int len = moneys.length;
        int[][] count = new int[len][2];
        count[0][0] = 0;
        count[0][1] = moneys[0];
        for (int i = 1; i < len; i++) {
            count[i][0] = Math.max(count[i - 1][0], count[i - 1][1]);
            count[i][1] = count[i - 1][0] + moneys[i];
        }
        return Math.max(count[len - 1][0], count[len - 1][1]);
    }
}
