package heap;

public class Main {
    public static void main(String[] args) {
        MaxHeap maxHeap = new MaxHeap(new int[]{4, 16, 14, 7, 1, 2, 8, 3, 10, 9});
        maxHeap.displayMaxHeap();
        maxHeap.heapSort();
    }
}
