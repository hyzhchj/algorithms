package permutation;

import java.util.Stack;

public class Main1 {
    public static void main(String[] args) {
        permutation(new int[]{1, 2, 3, 4, 5}, new Stack<Integer>());
    }

    //dfs+递归的方法
    private static void permutation(int[] array, Stack<Integer> stack) {
        if (array.length <= 0) {
            System.out.println(stack);
        } else {
            for (int i = 0; i < array.length; i++) {
                int[] arrayTemp = new int[array.length - 1];
                System.arraycopy(array, 0, arrayTemp, 0, i);
                System.arraycopy(array, i + 1, arrayTemp, i, array.length - i - 1);
                stack.push(array[i]);
                permutation(arrayTemp, stack);
                stack.pop();
            }
        }
    }
}
