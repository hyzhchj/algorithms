package permutation;

import java.util.Arrays;

public class Main2 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5};
        permutation(array, 0, array.length - 1);
    }

    //交换+递归的方法
    private static void permutation(int[] array, int start, int end) {
        if (start == end) {
            System.out.println(Arrays.toString(array));
        } else {
            for (int i = start; i <= end; i++) {
                swap(array, start, i);
                permutation(array, start + 1, end);
                swap(array, start, i);
            }
        }
    }

    private static void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
